#!/bin/bash

OLD="$(ls 4420_*)"
NEW="$(ls grsecurity-*)"
NNEW="4420_$NEW"

sed -i -e "s:${OLD}:${NNEW}:" 0000_README

mv $NEW $NNEW
rm $OLD
