#!/usr/bin/perl

use strict ;
use LWP::Simple ; ;
use HTML::LinkExtor ;

my @upstream_url	=
(
	"http://grsecurity.net/download_stable.php",
	"http://grsecurity.net/test.php"
) ;

my $file_pattern	= "grsecurity-";

my @gpg_suffixes	= ( ".patch.sig" ) ;
my @allowed_suffixes	= ( ".patch" ) ;
push( @allowed_suffixes, @gpg_suffixes ) ;

my %currently_available = () ;

my $GPG = "/usr/bin/gpg" ;
my $RM = "/bin/rm";


sub sane
{
	my ( $name ) = @_ ;

	return 0 if $name eq "" ;
	return 0 if $name =~ / / ;

	my $got_suffix = 0 ;
	foreach my $suffix ( @allowed_suffixes )
	{
		$got_suffix = 1 if $name =~ /$suffix$/ ;
	}

	return $got_suffix ;
}


sub get_currently_available
{
	my $parser ;
	my @links  ;

	foreach my $uurl ( @upstream_url )
	{
		$parser = HTML::LinkExtor->new( undef, $uurl ) ;
		$parser->parse( get( $uurl ) )->eof ;

		@links = $parser->links ;

		foreach my $ref ( @links )
		{
			my $file_url    = ${$ref}[2] ;
			my $file_name   = $file_url ;
			$file_name      =~ s/^.*\/(.*)$/$1/ ;

			next unless sane( $file_name ) ;

			$currently_available{ $file_name } = $file_url ;
		}
	}
}


sub download_newly_available
{
	my @downloads = () ;

	foreach my $file_name ( sort keys %currently_available )
	{
		next unless $file_name =~ /$file_pattern/ ;
		print "\tDownloading $file_name ... " ;
		my $file_url = $currently_available{ $file_name } ;
		if ( getstore( $file_url, $file_name ) )
		{
			print "OK\n" ;
			push(@downloads,$file_name);
		}
		else
		{
			print "FAIL\n" ;
		}
	}

	return @downloads ;
}


sub print_successful_downloads
{
	my @downloads = @_ ;

	if( $#downloads >= 0 )
	{
		print "\n\nSuccessfully downloaded files from upstream:\n\n" ;
		foreach( @downloads )
		{
			print "\t". $_ . "\n" ;
		}
		print "\n\n" ;
	}
	else
	{
		print "\n\nNo files downloaded from upstream --- nothing to report.\n\n" ;
		print "\n\n" ;
	}
}

sub test_gpg_sigs
{
	my @downloads = @_ ;

	print "\n\nTesting gpg sigs ...\n\n" ;
	foreach my $d ( @downloads )
	{
		foreach my $s ( @gpg_suffixes )
		{
			if( $d =~ /$s$/)
			{
				system("$GPG --verify $d >/dev/null 2>&1") ;
				my $err = $? >> 8 ;
				if( $err != 0 )
				{
					if( $err == 1 )
					{
						print "\tBAD signiture for $d\n" ;
					}
					else
					{
						print "\tUNKNOWN error for $d: $err\n" ;
					}
				}
				else
				{
					print "\tGOOD signiture for $d\n" ;
					system("$RM -f $d");
				}
			}
		}
	}
}


sub main
{
	get_currently_available() ;
	my @downloads = download_newly_available() ;

	print_successful_downloads( @downloads ) ;
	test_gpg_sigs( @downloads ) ;
}

main() ;
