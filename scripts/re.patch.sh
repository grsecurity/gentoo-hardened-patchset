#!/bin/bash

SCRIPT="$0"
KMAJOR="$1"
DIRVER="$(basename $(pwd) | sed -e 's/linux-//' -e 's/-hardened.*//')"
: ${KMAJOR:=$DIRVER}
PATCHSET="/root/hardened-patchset/${KMAJOR}"

if [[ ! -d ${PATCHSET} ]] ; then
	echo "Patchset ${KMAJOR} doesn't exist"
	exit
fi

for p in $(ls ${PATCHSET} | sort -r) ; do
	if [[ ${p#0000} == $p ]] ; then
		echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
		echo
		echo $p
		echo
		echo "Cont?"
		read n
		echo
		patch -p 1 -R < ${PATCHSET}/$p
		echo
		echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	fi
done

