#!/bin/bash

SCRIPT="$0"
KMAJOR="$1"
DIRVER="$(basename $(pwd) | sed -e 's/linux-//' -e 's/-hardened.*//')"
: ${KMAJOR:=$DIRVER}
PATCHSET="/root/hardened-patchset/${KMAJOR}"

if [[ ! -d ${PATCHSET} ]] ; then
	echo "Patchset ${KMAJOR} doesn't exist"
	exit
fi

for p in $(ls ${PATCHSET}) ; do
	if [[ ${p#0000} == $p ]] ; then
		echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
		echo
		echo $p
		echo
		patch -p 1 --dry-run < ${PATCHSET}/$p
		echo "Cont?"
		read n
		echo
		patch -p 1 < ${PATCHSET}/$p
		echo
		echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	fi
done

